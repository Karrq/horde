from llama_cpp import Llama
import sys
import threading
import queue

class Model:
    def __init__(self, model_path):
        self.model_path = model_path
        self.llm = Llama(model_path=self.model_path, seed=0)
        self.busy = False
        self.tokens = []

    def add(self, text):
        tokens = self.llm.tokenize(text.encode('utf-8'))
        self.llm.eval(tokens)

    def clear(self):
        self.llm.reset()

    def next(self):
        while True:
            token = self.llm.sample()
            if token == self.llm.token_eos():
                return None

            self.tokens.append(token)
            self.llm.eval([token])

            render = self.llm.detokenize(self.tokens).decode('utf-8')
            if render:
                self.tokens = []
                return render


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Model path argument required")
        sys.exit(1)

    model_path = sys.argv[1]
    model = Model(model_path)

    line_queue = queue.Queue()
    cancel_count = 0

    def write_out (text):
        sys.stdout.write(text)
        sys.stdout.flush()

    def main_thread_fn ():
        global cancel_count
        while True:

            if cancel_count > 0:
                for i in range(0, cancel_count):
                    write_out("<|lm_cancel|>")
                cancel_count = 0

            line = line_queue.get()

            if line == "<|lm_start|>":
                while cancel_count == 0:
                    token = model.next()
                    if token == None: break
                    write_out(token)
                write_out("<|lm_end|>")

            elif line == "<|lm_clear|>":
                model.clear()
            else:
                model.add(line.replace("<br>", "\n"))

    main_thread = threading.Thread(target=main_thread_fn, daemon=True)
    main_thread.start()

    write_out("<|lm_ready|>")

    for line in sys.stdin:
        line = line.rstrip('\n')

        # Cancels are asynchronous
        if line == "<|lm_cancel|>":
            cancel_count += 1
        else:
            line_queue.put(line)
