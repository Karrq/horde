import Model from "../model";
import {reactive} from "vue";

type EventID = number;
type ObjectRef = string | number | null;
type ContentFormat = string | null;

export class Event {
    // Index to event
    previous: EventId;
    content: string;
    format: ContentFormat;

    constructor (public content) {}
}

export class Message extends Event {
    sender: string;
    type: "message" = "message";

    constructor (
        public sender,
        content,
    ) {
        super(content)
    }
}

export class Chat extends Model {
    name: string;
    events: Event[] = [];
    head: EventID | null;
    // ["main"] contains the main route of conversation
    tags: Map<string, EventID> = {};

    agentId: ObjectRef;
    taskId: ObjectRef;

    main (): Event | null {
        let index = this.tags["main"]
        if (index != null) {
            return this.events[index];
        } else {
            return null;
        }
    }

    addEvent (content): Message {
        let msg = new Event(content);
        msg.previous = this.head;

        let eventId = this.events.length;
        reactive(this).events.push(msg);
        reactive(this).head = eventId;

        return msg;
    }

    addMessage (sender, content): Message {
        let msg = new Message(sender, content);
        msg.previous = this.head;

        let eventId = this.events.length;
        reactive(this).events.push(msg);
        reactive(this).head = eventId;

        return msg;
    }
}

Chat.configure({
    path: "chats",
    fields: {
        events (values) {
            return values.map(data => {
                return Object.assign(
                    data.type == "message" ?
                        new Message() :
                        new Event(),
                    data
                );
            });
        }
    }
});

export default Chat;
