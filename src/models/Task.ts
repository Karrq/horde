import Model from "../model";

import Chat from "./Chat";

class Task extends Model {
    slots = [];
    messages = [];
    logs = [];
    currentStepPath = [];

    get title () {
        return "Task #" + this.id;
    }

    async getChat () {
        return this.chatId != null ? Chat.get(this.chatId) : null;
    }

    async createChat () {
        let chat = Object.assign(await Chat.create(), {
            name: "Task " + this.id,
            taskId: this.id,
            agentId: this.agentId,
        });

        this.chatId = chat.id;
        return chat;
    }

    log (obj) {
        if (typeof obj == "string") {
            obj = {message: obj};
        }

        this.logs.push(obj);
    }
}

Task.configure({
    path: "tasks",
});

export default Task;