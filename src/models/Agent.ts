import Model from "../model";

import Task from "./Task";
import Script from "./Script";

class Agent extends Model {
    name: string;
    prompt: string;
    scriptId;

    get title () {
        return this.name || `Agent #${this.id}`;
    }

    async start (scriptId) {
        if (scriptId == null) {
            scriptId = this.scriptId;
            if (scriptId == null) {
                throw new Error("Missing script from argument or from agent")
            }
        }

        let script = await Script.get(scriptId);

        let task = await Task.create();
        task.scriptId = scriptId;
        task.agentId = this.id;

        return task;
    }
}

Agent.configure({
    path: "agents",
});

export default Agent;