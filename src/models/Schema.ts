
import Plugin from "./Plugin";
import Script from "./Script";

let actions = {
    google: {
        inputs: [{
            name: 'query',
        }],
    }
};

// discriminated on the type
let schemas = {
    "message": {
        inputs: [
            {
                name: 'from',
                format: 'enum',
                values: ['system', 'user', 'agent', 'input']
            },
            {name: 'content', format: 'long'}
        ],
    },
    "infer-message": {
        inputs: [{
            name: 'from',
            format: 'enum',
            values: ['system', 'user', 'agent']
        }],
        outputs: [{name: 'content'}]
    },
    "clear": {
        inputs: [],
    },


    "chat-write": {
        inputs: [
            {name: 'content', format: 'long'},
            {name: 'format', format: 'enum', values: ['', 'md']}
        ]
    },
    "chat-event": {
        inputs: [
            {name: 'content', format: 'long'},
            {name: 'format', format: 'enum', values: ['', 'md', 'choice']}
        ]
    },
    "chat-read": {
        inputs: [],
        outputs: [{name: 'content'}]
    },
    "glm-setup": {
        inputs: [
            {name: 'model', format: 'enum', values: [
                'gpt-3.5-turbo'
            ]},
        ]
    },

    "glm-save": {
        outputs: [{name: 'snapshot'}]
    },
    "glm-restore": {
        inputs: [{name: 'snapshot'}]
    },


    // Branching
    "if": {
        inputs: [{name: 'condition'}],
        branches: ['true', 'false'],
    },

    // Some action nodes
    "javascript": {
        inputs: [
            {name: 'script', format: 'long', constant: true},
        ]
    },
    "expression": {
        inputs: [
            {name: 'expression', format: 'long'},
        ],
        outputs: [{name: 'result'},],
    },
    "template": {
        inputs: [
            {name: 'template', format: 'long', constant: true},
        ],
        outputs: [{name: 'result'}],
        computed: (inputs) => {
            //let t = template.parse(inputs.template);
            console.log("template.computed", inputs);
            return {};
        }
    },
    "file-read": {
        inputs: [{name: 'path'}],
        outputs: [{name: 'content', format: 'long'}],
    },
    "file-write": {
        inputs: [{name: 'path'}, {name: 'content', format: 'long'}],
    },
    "dir-list": {
        inputs: [{name: 'path'}],
    },

    "plugin": {
        inputs: [],
        async computed (inputs) {
            let schema = {
                inputs: [{
                    name: 'plugin',
                    format: 'enum',
                    values: Plugin.listReactive().map(p=>p.title),
                }],
                outputs: [],
            };

            if (inputs.plugin != null) {
                let plugin = await Plugin.get_with_key_fn(inputs.plugin, (plugin) => plugin.title);

                schema.inputs.push({
                    name: 'member',
                    format: 'enum',
                    values: plugin.members.map(m => m.name),
                });

                if (inputs.member != null) {
                    let member = plugin.members.find(
                        m => m.name == inputs.member
                    );
                    if (member) {
                        schema.inputs.push(...member.inputs);
                        schema.outputs.push(...member.outputs);
                    }
                }
            }

            return schema;
        },
    },
    "script": {
        inputs: [],
        async computed (inputs) {
            let schema = {
                inputs: [{
                    name: 'script',
                    format: 'enum',
                    values: Script.listReactive().map(p=>p.title),
                }],
                outputs: [],
            };

            if (inputs.script != null) {
                let script = Script.get_with_key_fn(inputs.script, (script) => script.title);

                console.log("Script Schema", script);

                for (let slot of script.slots) {
                    if (slot.isInput) {
                        schema.inputs.push({
                            name: slot.key,
                        });
                    }

                    if (slot.isOutput) {
                        schema.outputs.push({
                            name: slot.key,
                        });
                    }
                }
            }

            return schema;
        },
    },
};

class Schema {
    static async compute (key, _inputs) {
        let schema = Schema.get(key);

        let inputs = {};
        for (let k in _inputs) {
            inputs[k] = _inputs[k].value;
        }

        if (schema.computed) {

            schema = Object.assign({}, schema);
            let computed = await schema.computed(inputs);

            if (computed.inputs) {
                schema.inputs = schema.inputs ?
                    Array.from(schema.inputs) : [];
                schema.inputs.push(...computed.inputs);
            }

            if (computed.outputs) {
                schema.outputs = schema.outputs ?
                    Array.from(schema.outputs) : [];
                schema.outputs.push(...computed.outputs);
            }
        }

        return schema;
    }
    static get (key) {
        return schemas[key] || {};
    }
    static list () {
        return Object.entries(schemas).map(([key,value])=>({key,value}));
    }
}

export default Schema;
