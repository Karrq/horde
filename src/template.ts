
export function parse (text) {
    let inputs = new Set();
    let segments = [];

    // TODO: This parsing limits the possible parsable expression

    while (text) {
        let tagIndex = text.indexOf("${");

        if (tagIndex >= 0) {
            if (tagIndex > 0) {
                segments.push({
                    type: "text",
                    value: text.slice(0, tagIndex),
                });
            }

            let endIndex = text.indexOf("}", tagIndex+2);

            if (endIndex < 0) {
                console.error("Malformed Template");
                segments = [{type: "text", value: text}];
                break;
            }

            let value = text.slice(tagIndex + 2, endIndex);
            segments.push({type: "expr", value});
            inputs.add(value);


            text = text.slice(endIndex + 1);
        } else {
            segments.push({type: "text", value: text});
            break;
        }
    }

    return {
        segments,
        inputs: Array.from(inputs),
        render (inputs) {
            let result = "";

            for (let segment of this.segments) {
                if (segment.type == "text") {
                    result += segment.value;
                }
                if (segment.type == "expr") {
                    let value = inputs[segment.value];
                    if (value != null) {
                        result += value;
                    }
                }
            }

            return result;
        },
    };
}

function test () {
    let raw = "Hello ${name}! Having ${age} years, how's ${place}?";
    let temp = parse(raw);
    let render = temp.render({name: "Jose", age: 13, place: "the hood"});
    console.log('template', render);
}

//test();