
import { ref, watch, computed } from "vue";
import { listen } from '@tauri-apps/api/event'

import * as config from "./config";
import * as env from "./env";

const models = [];

export default class Model {
    static listReactive () {
        return this._entriesReactive.value;
    }

    static create () {
        let entry = new this();

        let entries = this._entries;
        while (entries.has(String(this._nextId))) {
            this._nextId++;
        }

        entry.id = String(this._nextId++);

        entries.set(entry.id, entry);
        this._updateReactive();

        return entry;
    }

    static get (id) {
        if (id == null || id === '') {
            return new this();
        }

        id = String(id);

        let entries = this._entries;

        if (entries.has(id)) {
            return entries.get(id);
        } else {
            let entry = new this();
            entry.id = id;
            entries.set(id, entry);
            this._updateReactive();
            return entry;
        }
    }

    static get_with_key_fn<T> (needle: T, key_fn: (arg0: any) => T) {
        let entries = this._entries;

        for (let obj of entries.values()) {
            let key = key_fn(obj);
            if (key == needle) {
                return obj;
            }
        }

        return null;
    }

    static getReactive(getter) {
        let r = ref(new this());

        watch(getter, async newval => {
            console.log("newval", newval)
            let it = await this.get(newval);
            console.log("it", it)
            if (it) r.value = it;
        }, {immediate: true});

        return r;
    }

    static optionsReactive () {
        return computed(() => {
            let _entries = this._entriesReactive.value;
            let modelName = this.name;
            console.log('options reactive', _entries);

            return Object.fromEntries(_entries.map(
                it => [it.id, it.name || it.title || `${modelName} #${script.id}`]
            ));
        });
    }

    static delete (id) {
        let entries = this._entries;
        entries.delete(String(id));
        this._updateReactive();
    }

    static async loadAll () {
        // it's okay if it fails, it will be created automatically on write
        let fileList = await env.listDir(this._storePath) || [];

        let highestId = -1;

        let promResult = await Promise.all(fileList.map(async filename => {

            let extension = "." + (this._config.extension || "json");

            if (!filename.endsWith(extension)) {
                return null;
            }

            let id = filename.slice(0, -extension.length);
            let intid = parseInt(id);

            if (isFinite(intid) && intid > highestId) {
                highestId = intid;
            }

            // read File
            let fullpath = this._storePath + "/" + filename;
            let content = await env.readFile(fullpath);

            var it;
            if (this._config.parse) {
                it = this._config.parse(content, id);
            } else {
                let json = JSON.parse(content);

                // Create object
                it = Object.assign(new this(), json);

                for (let key in this.fieldMaps) {
                    let map = this.fieldMaps[key];
                    it[key] = map(it[key]);
                }
            }

            // Ignore what the file says lol
            it.id = id;
            return it;
        }));

        let entries = new Map();

        for (let entry of promResult) {
            if (!entry) continue;

            entries.set(entry.id, entry);
        }

        this._nextId = highestId + 1;
        this._entries = entries;
        this._updateReactive();
    }

    static _updateReactive () {
        this._entriesReactive.value = Array.from(this._entries.values());
    }

    static configure (params) {
        this._path = params.path;
        this._storePath = ".horde/" + this._path;

        this._config = params;

        this.fieldMaps = {};
        if (params.fields) {
            Object.assign(this.fieldMaps, params.fields);
        }

        this._entries = new Map();
        this._entriesReactive = ref([]);

        // Only take the list when there's a project path.
        // We don't care about the specific path, env takes care of it
        config.onProjectPathChange(async _newPath => {
            this.loadAll()
        });

        models.push(this);
    }

    static async saveAll () {
        let entries = this._entries;

        // encode is not opposite of parse...
        if (this._config.parse && !this._config.encode) {
            // Model can't be saved if parse and encode don't match.
            console.log("Can't save " + this.name);
            return;
        }

        let values = Array.from(entries.values());
        await Promise.all(values.map(async entry => {
            let ext = this._config.extension || "json";

            let filePath = `${this._storePath}/${entry.id}.${ext}`;
            let encoded;
            if (this._config.encode) {
                encoded = this._config.encode(entry);
            } else {
                encoded = JSON.stringify(entry, "\n", "  ");
            }
            await env.writeFileAdmin(filePath, encoded);
        }));

        console.log("Saved %d entries (%s)", entries.size, this._storePath);
    }
}

listen("save", () => {
    for (let model of models) {
        model.saveAll();
    }
});
