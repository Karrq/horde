import * as dialog from '@tauri-apps/api/dialog';

import { emit } from '@tauri-apps/api/event'

export async function confirm (prompt) {
    return window.confirm(prompt);
}

export async function input (prompt) {
    return window.prompt(prompt);
}

export async function inputFile (options) {
    return await dialog.open(options);
}

export function alert (message) {
    emit("alert", {message});
}