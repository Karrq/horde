
import { Store } from "tauri-plugin-store-api";

const STORE = new Store("config");
var opening = false;

const config = {
    projectPath: null as string | null,
};

var listeners = {
    projectPath: [],
};



export function get (key) {
    return config[key];
}

export function set (key, val) {
    config[key] = val;
    for (let fn of listeners[key] || []) {
        fn(val);
    }
    saveAll();
}

export function watch (key, fn) {
    (listeners[key] ||= []).push(fn);

    let val = get(key);
    if (val != null) fn(val);
}



export function getProjectPath () {
    return config.projectPath;
}

export function setProjectPath (path) {
    // Check it exists
    config.projectPath = path;
    for (let fn of listeners.projectPath) {
        fn(path);
    }
    saveAll();
}

export function onProjectPathChange (fn) {
    listeners.projectPath.push(fn);
    if (config.projectPath != null) {
        fn(config.projectPath);
    }
}

async function saveAll () {
    if (opening) return;

    console.log("save config", config);
    await STORE.set("config", config);
    await STORE.save();
}

STORE.get("config").then(newConfig => {
    if (newConfig instanceof Object) {
        console.log("load config", newConfig);
        opening =  true;

        for (var key in newConfig) {
            set(key, newConfig[key]);
        }

        /*
        Object.assign(config, newConfig);
        for (let key in newConfig) {
            if (!listeners[key]) continue;

            for (let fn of listeners[key]) {
                fn(newConfig[key]);
            }
        }
        */

        opening =  false;
    }
});
