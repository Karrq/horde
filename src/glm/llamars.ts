
import { invoke } from "@tauri-apps/api/tauri";
import { listen } from '@tauri-apps/api/event'

const models = {
  orca_mini: {
    path: "../models/orca_mini.bin",
    //path: "../../horde1/models/ggml-model-q4_0.bin",
    roles: {
      system: "System",
      user: "User",
      agent: "Response",
      input: "Input",
    },
    format: (role, content) => `### ${role}:\n${content}\n`,
    //format: (role, content) => content,
  }
};

// Select the model
const model = models.orca_mini;

const Llama = {
  async setup () {
    await invoke("llama_setup", {modelPath: model.path});
  },

  async addMessage (role, content) {
    let roleName = model.roles[role];
    if (!roleName) {
      console.error("Unknown role: " + role);
      return;
    }

    console.log("message", role, content);

    let text = model.format(roleName, content);
    let value = await invoke("llama_add_text", {content: text});
  },

  async send (callback) {

    let value = await invoke("llama_add_text", {content: "### Response:\n"});
    let response = "";

    var eofResolve = null;
    var eofPromise = new Promise((resolve, reject) => {
      eofResolve = resolve;
    });

    const unlistenToken = await listen("llama-token", event => {
      let token = event.payload.token;
      console.log('llama-token', event);
      callback(token);
      response += token;
    });

    const unlistenEof = await listen("llama-eof", event => {
      console.log('llama-eof', event);
      eofResolve();
    });

    await invoke("llama_run", null);
    await eofPromise;

    unlistenToken();
    unlistenEof();

    return response;
  },

  async clear () {
    await invoke("llama_clear", null);
  },

  async cancel () {
    await invoke("llama_cancel", null); 
  }
};

export default Llama;