
import * as config from "../config";

const snapshots = [];

const ChatGPT = {
  apiKey: null,
  model: "gpt-3.5-turbo",
  messages: [],
  cancelled: false,

  setup () {
    console.log('chatgpt setup')
    config.watch('openai-token', val => {
      console.log("chatgpt token changed");
      this.apiKey = val;
    });
  },

  clear () {
    this.messages = [];
  },

  addMessage (role, content) {
    if (role == 'agent') role = 'assistant';
    if (!['system', 'user', 'assistant'].includes(role)) return;

    this.messages.push({role, content});
  },

  cancel () {
    this.cancelled = true;
  },

  async send (callback) {
    // Response handling from:
    // https://github.com/ztjhz/BetterChatGPT
    // https://github.com/dubisdev/quickgpt

    function parseEventSource (data) {
      const result = data
        .split('\n\n')
        .filter(Boolean)
        .map((chunk) => {
          const jsonString = chunk
            .split('\n')
            .map((line) => line.replace(/^data: /, ''))
            .join('');
          if (jsonString === '[DONE]') return jsonString;
          try {
            const json = JSON.parse(jsonString);
            return json;
          } catch {
            return jsonString;
          }
        });
      return result;
    };

    let streaming = !!callback;

    let response = await fetch('https://api.openai.com/v1/chat/completions', {
      //signal: abortController.signal,
      method: 'POST',
      headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${ChatGPT.apiKey}`
      },
      body: JSON.stringify({
        model: ChatGPT.model,
        stream: streaming,
        messages: ChatGPT.messages
      })
    });

    if (!response.ok) {
      //log("Request Failed");
      return;
    }

    if (streaming) {
      var fullContent = "";

      const reader = response.body?.getReader();
      if (!reader) throw new Error("No stream received from OpenAI");

      let partial = '';
      while (true) {
        if (this.cancelled) {
          reader.cancel();
          break;
        }

        const { done, value } = await reader.read();

        const result = parseEventSource(
          partial + new TextDecoder().decode(value)
        );
        partial = '';

        if (result === '[DONE]' || done) {
          break;
        } else {
          const resultString = result.reduce((output: string, curr) => {
            if (typeof curr === 'string') {
              partial += curr;
            } else {
              const content = curr.choices[0].delta.content;
              if (content) output += content;
            }
            return output;
          }, '');

          callback(resultString);
          fullContent += resultString;
        }
      }

      if (this.cancelled) {
        this.cancelled = false;
      } else {
        ChatGPT.messages.push({role: 'assistant', content: fullContent});
      }
      return fullContent;
    } else {
      let data = await response.json();
      let message = data.choices[0].message;

      ChatGPT.messages.push(message);
      return message.content;
    }
  },

  save () {
    let snapshotId = snapshots.length;
    snapshots.push(Array.from(this.messages));
    return snapshotId;
  },

  restore (snapshotId) {
    let snapshot = snapshots[snapshotId];
    if (snapshot == null) {
      console.error("Invalid snapshot", snapshotId);
    } else {
      this.messages = snapshot;
      console.log(
        "snapshot %s restored", snapshotId,
        this.messages.map(msg => msg.role + ":" + msg.content).join("\n")
      );
    }
  }
};

export default ChatGPT;