// Prevents additional console window on Windows in release, DO NOT REMOVE!!
#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]

use std::{fs, path::Path};
use std::sync::{Mutex};

use tauri::{State, Window, command};

mod glm;

struct AppState {
    glm: Mutex<Option<glm::Handle>>,
}



// === File System Stuff === //

trait ResultExt {
    type Inner;
    fn oklog (self) -> Option<Self::Inner>;
    fn is_oklog (self) -> bool;
}
impl<T, E: std::fmt::Display> ResultExt for Result<T, E> {
    type Inner = T;
    fn oklog (self) -> Option<T> {
        match self {
            Ok(x) => Some(x),
            Err(e) => {
                println!("error: {}", e);
                None
            }
        }
    }
    fn is_oklog (self) -> bool {
        match self {
            Ok(_) => true,
            Err(e) => {
                println!("error: {}", e);
                false
            }
        }
    }
}

#[command]
fn read_file (window: Window, file_path: &str) -> Option<String> {
    println!("read file: {}", file_path);
    fs::read_to_string(file_path).oklog()
}

#[command]
fn write_file (window: Window, file_path: &str, content: &str) -> bool {
    println!("write file: {}", file_path);

    let parent = match Path::new(file_path).parent() {
        Some(parent) => parent,
        None => {
            println!("error: no parent? {:?}", file_path);
            return false;
        }
    };

    fs::create_dir_all(parent).is_oklog() &&
    fs::write(file_path, content).is_oklog()

}

#[command]
fn list_dir (window: Window, dir_path: &str) -> Option<Vec<String>> {
    println!("list dir: {}", dir_path);
    fs::read_dir(dir_path).oklog().map(|iter| {
        iter
            .filter_map(Result::ok)
            .filter_map(|dir_entry| {
                dir_entry
                    .file_name()
                    .into_string()
                    .ok()
            })
            .collect()
    })
}




// Clone and Serialize necessary for events.
#[derive(Clone, serde::Serialize)]
struct CommandResultEvent {
  id: String,
  exit_code: String,
  stdout: String,
}

#[command]
fn cmd (window: Window, id: String, args: Vec<String>) {
    println!("cmd: {:?}", args);

    std::thread::spawn(move || {
        use std::process::{Command, Child, Stdio};
        use std::io::{Write, Read};

        let mut cmd = Command::new(&args[0]);
        for arg in args.iter().skip(1) {
            cmd.arg(arg);
        }
        let output = cmd.output().expect("failed to execute");

        let result = CommandResultEvent {
            id: id.to_owned(),
            exit_code: format!("{}", output.status),
            stdout: String::from_utf8_lossy(&output.stdout).into(),
        };

        window.emit("cmd-result", result).unwrap();
    });
}



// === GLM Stuff === //

// Clone and Serialize necessary for events.
#[derive(Clone, serde::Serialize)]
struct TokenEvent {
  token: String,
}

fn process_response (window: &Window, res: glm::Response) {
    match res {
        glm::Response::Inference(t) => {
            window.emit("llama-token", TokenEvent {token: t}).unwrap();
        },
        glm::Response::Eof => {
            window.emit("llama-eof", ()).unwrap();
        },
        glm::Response::Cancelled => {
            window.emit("llama-cancelled", ()).unwrap();
        },
    }
}

#[command]
fn llama_setup (window: Window, model_path: &str, state: State<AppState>) {
    let mut glm_lock = state.glm.lock().unwrap();

    if glm_lock.is_none() {
        let handle = glm::setup(model_path, move |res| {
            process_response(&window, res);
        });

        *glm_lock = Some(handle);
    } else {
        println!("Model already setup");
    }
}

#[command]
fn llama_add_text (content: &str, state: State<AppState>) {
    // TODO: Get the glm handle from the state and send

    match state.glm.lock().unwrap().as_mut() {
        Some(state) => {
            state.add_text(content.to_string());
        },
        None => {
            println!("No model loaded");
        }
    }
}

#[command]
fn llama_run (state: State<AppState>) {
    match state.glm.lock().unwrap().as_mut() {
        Some(state) => {
            state.start_inference();
        },
        None => {
            println!("No model loaded");
        }
    }
}

#[command]
fn llama_clear (state: State<AppState>) {
    if let Some(glm) = state.glm.lock().unwrap().as_mut() {
        glm.clear();
    }
}

#[command]
fn llama_cancel (state: State<AppState>) {
    if let Some(glm) = state.glm.lock().unwrap().as_mut() {
        glm.cancel();
    }
}

#[command]
fn open_debug (window: Window) {
    println!("open_debug");
    window.open_devtools();
}

fn main() {
    tauri::Builder::default()
        .plugin(tauri_plugin_store::Builder::default().build())
        .manage(AppState{glm: Mutex::new(None)})
        .invoke_handler(tauri::generate_handler![
            open_debug,
            read_file, write_file, list_dir, cmd,
            llama_setup, llama_add_text, llama_run, llama_clear, llama_cancel
        ])
        .run(tauri::generate_context!())
        .expect("error while running tauri application");
}
