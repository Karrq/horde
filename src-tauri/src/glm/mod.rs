//! Generative Language Model

use std::sync::mpsc::{self, Sender, Receiver};

//mod llamars;
mod llamapy;

trait Glm {
    /// Does all necessary initialization and loading for the model.
    /// Called from the glm thread.
    fn setup (&mut self) {}

    /// Clears the history/state of the model and start inference from nothing.
    fn clear (&mut self);

    /// Add text to the inference prompt.
    fn add_text (&mut self, text: &str);

    /// Generates the next text prediction, could be one token or a batch.
    /// Returns None when the model predicts the end of the text.
    fn infer_next (&mut self) -> Option<String>;

    /// Stops inference if its running.
    fn cancel (&mut self) {}
}

#[derive(Clone, Debug)]
pub enum Request { AddText(String), StartInference, StopInference, Clear }

#[derive(Clone, Debug)]
pub enum Response { Inference(String), Eof, Cancelled }

pub struct Handle {
    sender: Sender<Request>,
}

impl Handle {
    pub fn clear (&self) {
        self.sender.send(Request::Clear).unwrap();
    }

    pub fn cancel (&self) {
        self.sender.send(Request::StopInference).unwrap();
    }

    pub fn add_text (&self, text: String) {
        self.sender.send(Request::AddText(text)).unwrap();
    }

    pub fn start_inference (&self) {
        self.sender.send(Request::StartInference).unwrap();
    }
}

fn start_loop (
    mut state: impl Glm,
    rx: Receiver<Request>,
    callback: impl Fn(Response),
) {
    state.setup();
    println!("glm event loop started");

    let mut queue = std::collections::VecDeque::new();

    loop {
        let req = queue.pop_front().or_else(|| rx.recv().ok());
        if let Some(req) = req {

            match req {
                Request::AddText(text) => {
                    state.add_text(&text);
                },
                Request::StartInference => {
                    while let Some(text) = state.infer_next() {
                        callback(Response::Inference(text));

                        match rx.try_recv() {
                            Ok(Request::StopInference) => {
                                state.cancel();
                                callback(Response::Cancelled);

                                // Break inference loop
                                break;
                            },
                            Ok(req) => {
                                queue.push_back(req);
                            },
                            Err(mpsc::TryRecvError::Disconnected) => {
                                // Channel dead
                                break;
                            },
                            Err(mpsc::TryRecvError::Empty) => {
                                // Continue the loop as if nothing
                            },
                        }
                    }
                    callback(Response::Eof);
                },
                Request::Clear => {
                    state.clear();
                },
                _msg => {
                    println!("Unknown message");
                }
            }
        } else {
            // No more in queue and channel closed
            break;
        }
    }
}

pub fn setup (
    model_path: &str,
    callback: impl Fn(Response) + Send + 'static,
) -> Handle {
    //let state = llamars::State::load(model_path).unwrap();
    let state = llamapy::State::load(model_path);

    let (tx, rx) = mpsc::channel::<Request>();
    std::thread::spawn(move || {
        start_loop(state, rx, callback);
    });

    return Handle { sender: tx }
}