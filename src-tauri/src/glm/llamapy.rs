use std::process::{Command, Child, Stdio};
use std::io::{Write, Read};

pub struct State {
    busy: bool,
    child: Child,
}

impl State {
    pub fn load (model_path: &str) -> State {
        let child = Command::new("/bin/python")
            .arg("../llamacpp")
            .arg(model_path)
            .stdin(Stdio::piped())
            .stdout(Stdio::piped())
            .stderr(Stdio::inherit())
            .spawn()
            .expect("failed to execute child");

        State{ child, busy: false }
    }

    fn read (&mut self) -> Option<String> {
        let stdout = self.child.stdout.as_mut().unwrap();

        let mut buffer = [0u8; 512];

        if let Ok(redd) = stdout.read(&mut buffer) {
            match std::str::from_utf8(&buffer[0..redd]) {
                Ok(s) => Some(s.to_string()),
                Err(_) => Some(String::new()),
            }
        } else {
            None
        }
    }
}


impl crate::glm::Glm for State {
    fn setup (&mut self) {
        assert_eq!(
            self.read(),
            Some("<|lm_ready|>".to_owned())
        );
    }

    fn add_text (&mut self, text: &str) {
        let stdin = self.child.stdin.as_mut().unwrap();

        println!("llama.py <- {}", text);

        // The python llama script reads by lines.
        // TODO: escape newlines, they are encoded as `<br>`
        stdin.write_all(text.as_bytes()).unwrap();
        stdin.write_all("\n".as_bytes()).unwrap();
    }

    fn clear (&mut self) {
        self.add_text("<|lm_clear|>");
    }

    fn infer_next (&mut self) -> Option<String> {
        if !self.busy {
            self.add_text("<|lm_start|>");
            self.busy = true;
        }

        let text = self.read()?;

        // TODO: Is it guaranteed that this keyword will
        // come complete in its own buffer packet?
        if text == "<|lm_end|>" {
            self.busy = false;
            return None;
        }

        Some(text)
    }

    fn cancel (&mut self) {
        self.add_text("<|lm_cancel|>");
    }
}