//! Model powered by llama-rs (main crate: llm)

use llm::{Model, InferenceSession, models::Llama};

pub struct State {
    model: Llama,
    session: InferenceSession,
    buffer: String,
}

impl State {
    pub fn load (model_path: &str) -> Option<State> {
        let path = std::path::Path::new(model_path);

        // load a GGML model from disk
        let model = match llm::load::<Llama>(
            path,
            llm::TokenizerSource::Embedded,
            // llm::ModelParameters
            Default::default(),
            llm::load_progress_callback_stdout
        ) {
            Ok(model) => model,
            Err(err) => {
                return None;
                //panic!("Failed to load model: {:?}", err);
            }
        };

        let session = model.start_session(Default::default());

        Some(State {
            model, session,
            buffer: String::new()
        })
    }
}

impl crate::glm::Glm for State {
    fn add_text (&mut self, text: &str) {
        self.session.feed_prompt(
            &self.model,
            llm::Prompt::from(text),
            &mut llm::OutputRequest::default(),
            |bytes| {
                // Here I could report the prompt tokens processed by the lm
                // and stop if I get a stop signal, with ::Halt
                Ok::<_, std::convert::Infallible>(llm::InferenceFeedback::Continue)
            },
        ).unwrap();
    }

    fn infer (&mut self) -> Option<String> {
        let mut buf = llm::TokenUtf8Buffer::new();

        loop {
            let res = self.session.infer_next_token(
                &self.model,
                &llm::InferenceParameters::default(),
                &mut llm::OutputRequest::default(),
                &mut rand::thread_rng(),
            );

            match res {
                Ok(token_bytes) => {
                    if let Some(string) = buf.push(&token_bytes) {
                        return Some(string);
                    }
                },
                Err(llm::InferenceError::EndOfText) => return None,
                Err(e) => panic!("{:?}", e),
            }
        }
    }

    fn clear (&mut self) {}
}